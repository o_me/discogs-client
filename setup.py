from setuptools import setup, find_packages

NAME = "discogsclient"

VERSION = "0.1"

REQUIRES = [
    "discogs-client==2.2.2",
    "PyYAML==3.13",
]

setup(
    name=NAME,
    version=VERSION,
    description="Discogs Client",
    author_email="meyer.oliver93@gmail.com",
    install_requires=REQUIRES,
    packages=find_packages(),
)
