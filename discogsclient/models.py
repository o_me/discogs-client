import discogs_client
import yaml
import os


CONFIG_PATH = os.path.join(
    os.path.dirname(os.path.abspath(__file__)),
    'configs'
)


class DiscogsClient:

    def __init__(self):

        self.client = None

    def connect(self, config=None):

        if not config:
            config = os.path.join(
                CONFIG_PATH,
                'discogs.yml'
            )

        with open(config, 'r') as fh:
            config = yaml.load(fh)

        client = discogs_client.Client(
            user_agent=config['user-agent'],
            consumer_key=config['consumer-key'],
            consumer_secret=config['consumer-secret'],
            token=config['access-token'],
            secret=config['access-secret'],
        )

        self.client = client

    def get_release(self, release_id):

        data = self.client.release(release_id)

        artist_names = '--'.join([artist.name for artist in data.artists])
        artist_ids = '--'.join([str(artist.id) for artist in data.artists])
        title = data.title
        label_names = '--'.join([label.name for label in data.labels])
        label_ids = '--'.join([str(label.id) for label in data.labels])
        year = data.year
        country = data.country
        genres = '--'.join([genres for genres in data.genres])
        styles = '--'.join([styles for styles in data.styles] if data.styles else 'No style')

        data = {
            'artist_names': artist_names,
            'artist_ids': artist_ids,
            'title': title,
            'label_names': label_names,
            'label_ids': label_ids,
            'year': year,
            'country': country,
            'genres': genres,
            'styles': styles
        }

        return data
