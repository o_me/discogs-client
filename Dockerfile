FROM python:3.7

ENV CONFIG_PATH /discogs-client/discogsclient/configs

ADD requirements.txt /discogs-client/requirements.txt

WORKDIR /discogs-client

RUN pip install -r requirements.txt
